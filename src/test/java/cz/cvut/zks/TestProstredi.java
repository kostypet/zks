package cz.cvut.zks;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;

//Be careful in splitting the test code to individual @Tests:
//an execution plan of Tests is not guaranteed by default
//We need to use @FixMethodOrder(MethodSorters.{NAME_ASCENDING}) above unit test class declaration
//http://junit.org/apidocs/index.html?org/junit/runners/MethodSorters.html
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestProstredi {

    static RemoteWebDriver driver;

    @BeforeClass
    public static void beforeClass() {
        // System.setProperty("webdriver.chrome.driver", "C:\\chromedriver\\chromedriver.exe");
        // driver = new ChromeDriver();
        System.setProperty("webdriver.gecko.driver", "G:\\Documents\\School\\FEL\\M1\\ZKS\\geckodriver.exe");
        driver = new FirefoxDriver();
    }

    @AfterClass
    public static void afterClass() {
        //Pause.pause(2);	
        driver.quit();
    }

    @Test
    public void step1_loginTest() {
        driver.get("http://demo.redmine.org");
        driver.findElement(By.linkText("Sign in")).click();

        waitForElement(driver, By.id("username"));

        driver.findElement(By.id("username")).sendKeys("kostypet@fel.cvut.cz");
        driver.findElement(By.id("password")).sendKeys("kostypet@fel.cvut.cz");
        driver.findElement(By.name("login")).click();
        
        boolean isLogged = driver.findElement(By.id("loggedas")) != null;
        assertTrue("Didn't find info about logged user", isLogged);
        
        driver.findElement(By.className("logout")).click();
    }

    @Test
    public void step1_loginFailureTest() {
        driver.get("http://demo.redmine.org");
        driver.findElement(By.linkText("Sign in")).click();

        waitForElement(driver, By.id("username"));

        driver.findElement(By.id("username")).sendKeys("kostypet@fel.cvut.cz");
        driver.findElement(By.id("password")).sendKeys("4789tyn3qm9rdk09aw");
        driver.findElement(By.name("login")).click();
        
        boolean hasErrorMessage = driver.findElement(By.id("flash_error")).getText().trim().equals("Invalid user or password");
        assertTrue("Error message was not displayed", hasErrorMessage);
    }

    private void waitForElement(RemoteWebDriver driver, final By by) {
        Wait<WebDriver> wait = new WebDriverWait(driver, 10);
        wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return driver.findElement(by).isDisplayed();
            }
        });
    }

}
